## About Me :)

<img src="https://gitlab.com/yamunann/idp-project-199091/-/raw/main/IMG_5937_Original_Original_Original.jpeg" width=150 align=middle>

## Name
-  Yamunan Selvanathan

## Age
- 21

## Hometown
- Pasir Gudang, Johor Bahru, Johor

## Strength & Weakness
 First Header | Second Header
------------ | -------------
Determination | Self-criticism
Teamwork | Procrastination
Determination | 

## Countries I've Been To 
- Singapore
- Thailand 
- Indonesia 
- India 
- Australia
- Spain
- Russia 
- England

